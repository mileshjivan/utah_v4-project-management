/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;

import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author smabe
 */
public class S5_2_Project_Management_TestSuite {

    static TestMarshall instance;

    public S5_2_Project_Management_TestSuite() {
        ApplicationConfig appConfig = new ApplicationConfig();
        TestMarshall.currentEnvironment = Enums.Environment.coreBeta;
    }

    //FR1-Capture Project Management - Main Scenario
    @Test
    public void FR1_Capture_Project_Management_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Project Management v5.2\\FR1-Capture Project Management - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }     
    @Test
    public void FR1_Capture_Project_Management_OptionalScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Project Management v5.2\\FR1-Capture Project Management - Optional Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR2-Capture Forecasted Budget - Main Scenario
    @Test
    public void FR2_Capture_Forecasted_Budget_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Project Management v5.2\\FR2-Capture Forecasted Budget - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }  
    
    //FR3-Capture Project Actual - Main Scenario
    @Test
    public void FR3_Capture_Project_Actual_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Project Management v5.2\\FR3-Capture Project Actual - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }  
    
    //FR4-View Linked Engagements - Main Scenario
    @Test
    public void FR4_View_Linked_Engagements_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Project Management v5.2\\FR4-View Linked Engagements - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR5-Capture Project Actions  - Main Scenario
    @Test
    public void FR5_Capture_Project_Actions_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Project Management v5.2\\FR5-Capture Project Actions  - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
   
}
